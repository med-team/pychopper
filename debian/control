Source: pychopper
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-setuptools,
	       python3-sphinx <!nodoc>,
	       python3-sphinx-argparse <!nodoc>,
	       python3-sphinx-rtd-theme <!nodoc>,
               python3-all,
               python3-edlib <!nocheck>,
               python3-parasail <!nocheck>,
               python3-matplotlib <!nocheck>,
               python3-seaborn <!nocheck>,
               python3-tqdm <!nocheck>,
               python3-pandas <!nocheck>,
               python3-pytest <!nocheck>,
               python3-pytest-arraydiff <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-pytest-doctestplus <!nocheck>,
               python3-pytest-forked <!nocheck>,
               python3-pytest-openfiles <!nocheck>,
               python3-pytest-remotedata <!nocheck>,
               python3-pytest-timeout <!nocheck>,
               python3-pytest-xdist <!nocheck>,
               python3-hypothesis <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-pysam <!nocheck>,
               python3-zombie-imp <!nodoc>,
               xonsh <!nocheck>
Standards-Version: 4.6.2
Homepage: https://github.com/epi2me-labs/pychopper
Vcs-Browser: https://salsa.debian.org/med-team/pychopper
Vcs-Git: https://salsa.debian.org/med-team/pychopper.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-pychopper
Architecture: all
Section: python
Depends: ${python3:Depends}, ${misc:Depends}
Description: identify, orient and trim full-length Nanopore cDNA reads
 Pychopper v2 is a Python module to identify, orient and trim full-length
 Nanopore cDNA reads. It is also able to rescue fused reads and provides
 the script 'pychopper.py'.  The general approach of Pychopper v2
 is the following:
 .
  * Pychopper first identifies alignment hits of the primers across the
    length of the sequence. The default method for doing this is using
    nhmmscan with the pre-trained strand specific profile HMMs, included
    with the package. Alternatively, one can use the edlib backend,
    which uses a combination of global and local alignment to identify
    the primers within the read.
  * After identifying the primer hits by either of the backends, the
    reads are divided into segments defined by two consecutive primer
    hits. The score of a segment is its length if the configuration of
    the flanking primer hits is valid (such as SPP,-VNP for forward reads)
    or zero otherwise.
  * The segments are assigned to rescued reads using a dynamic programming
    algorithm maximizing the sum of used segment scores (hence the amount
    of rescued bases). A crucial observation about the algorithm is that
    if a segment is included as a rescued read, then the next segment
    must be excluded as one of the primer hits defining it was "used
    up" by the previous segment. This put constraints on the dynamic
    programming graph. The arrows in read define the optimal path for
    rescuing two fused reads with the a total score of l1 + l3.
 .
 A crucial parameter of Pychopper v2 is -q, which determines the
 stringency of primer alignment (E-value in the case of the pHMM
 backend). This can be explicitly specified by the user, however by
 default it is optimized on a random sample of input reads to produce
 the maximum number of classified reads.
